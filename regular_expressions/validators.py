# encoding: utf-8
import re

# Задача 1
class PasswordValidator(object):

    @staticmethod
    def validate1(string):
        # состоит ли код только из цифр и его длина как минимум четыре символа
        # \d - любая циара    {4,} - от 4 и более вхождений шаблона слева   $ - конец строки.
        return bool(re.match(r'\d{4,}$', string))

    @staticmethod
    def validate2(string):
        # состоит ли пароль, только из латинских строчных букв и его длина от 6 до 10 символов
        # [a-z] - строчные буквы от a до z    {6,10} от 6 до 10 вхождений   $ - до самого конца строки
        return bool(re.match(r'[a-z]{6,10}$', string))

    @staticmethod
    def validate3(string):
        # состоит ли пароль, только из латинских строчных букв, прописных букв, чисел и символов  ‘@’, ‘.’ ‘_’ -‘’
        # и его длина от 6 до 10 символов.
        # \w - любая латинская буква или цифра (эквивалент [a-zA-Z0-9_])    \. точка    \-  тире    $ - до конца строки
        return bool(re.match(r'[\w@\.\-]{6,10}$', string))

    @staticmethod
    def validate4(string):
        # состоит ли пароль, только из латинских строчных букв, прописных букв, чисел и символов  ‘@’, ‘.’ ‘_’ -‘’
        # и его длина от 6 до 10 символов. Присутствие всех типов символов требуется
        return re.match(r'[\w@\.\-]{6,10}$', string) and \
           re.search(r'[a-z]', string) and \
           re.search(r'[A-Z]', string) and \
           re.search(r'\d', string) and \
           bool(re.search(r'[_@\.\-]', string))


# Задача 2
def format_phone(string):
    result = re.sub(r'^\+7|^8', '', string)
    r = re.split('\D', result)
    return ''.join(r)


# Задача 3
def is_valid_email(string):
    #Адрес должен быть вида test@test.com, и может содержать только латинские буквы, цифры, знак подчёркивания и дефис.
    return bool(re.match(r'^[\w\-]+@[\w\-]+\.[\w\-]{2,}', string))

